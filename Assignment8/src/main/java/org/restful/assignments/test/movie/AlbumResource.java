package org.restful.assignments.test.movie;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

@Path("/albums")
public class AlbumResource {
	
	AlbumService ms = new AlbumService();
	
	@GET
	@Produces({MediaType.APPLICATION_XML,MediaType.APPLICATION_JSON,MediaType.TEXT_XML})
	public List<Album> getAlbums(){
		return ms.getAllAlbums();
	}	
}
