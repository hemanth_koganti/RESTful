package org.restful.assignments.test.movie;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

@Path("/movies")
public class MovieResource {
	
	MovieService ms = new MovieService();
	
	@GET
	@Produces({MediaType.APPLICATION_XML,MediaType.APPLICATION_JSON,MediaType.TEXT_XML})
	public List<Movie> getMovies(){
		return new ArrayList<Movie>(ms.getAllMovies());
	}
	
	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_XML)
	public Movie getMovie(@PathParam("id") String id) {
		return ms.getMovie(id);
	}
	
	
}
