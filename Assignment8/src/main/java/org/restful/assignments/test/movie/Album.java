package org.restful.assignments.test.movie;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Album {
	
	private String title;
	private String singer;
	
	public Album(String title,String singer) {
		this.title = title;
		this.singer = singer;
	}
	
	public Album() {
		
	}
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getSinger() {
		return singer;
	}
	public void setSinger(String singer) {
		this.singer = singer;
	}
		
}
