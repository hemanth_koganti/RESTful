package org.restful.assignments.test.movie;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class AlbumService {
	
	private List<Album> albums = new ArrayList<Album> (Arrays.asList(
			new Album("Reflection","Brian Emo"),
			new Album("No Plan","David Bowie"),
			new Album("Migration","Bonobo")
			));
	
	public List<Album> getAllAlbums() {
		 return albums;
	}

}
