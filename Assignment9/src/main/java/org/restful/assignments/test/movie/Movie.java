package org.restful.assignments.test.movie;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Movie {
	
	private String movieId ;
	private String movieName ;
	private String movieActor ;
	private Float movieCollection ;
	
	public Movie(String mid,String mname,String mactor,Float mClctn) {
		this.movieId = mid;
		this.movieName = mname;
		this.movieActor = mactor;
		this.movieCollection = mClctn;
	}
	
	public Movie() {
		
	}
	
	public String getMovieId() {
		return movieId;
	}
	public void setMovieId(String movieId) {
		this.movieId = movieId;
	}
	public String getMovieName() {
		return movieName;
	}
	public void setMovieName(String movieName) {
		this.movieName = movieName;
	}
	public String getMovieActor() {
		return movieActor;
	}
	public void setMovieActor(String movieActor) {
		this.movieActor = movieActor;
	}
	public Float getMovieCollection() {
		return movieCollection;
	}
	public void setMovieCollection(Float movieCollection) {
		this.movieCollection = movieCollection;
	}
	
}
