package org.restful.assignments.test.movie;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MovieService {
	
	private List<Movie> movies = new ArrayList<Movie> (Arrays.asList(
			new Movie("1","John Wick","Keenu Reaves",400000.0F),
			new Movie("2","Batman Begins","Christian Bale",800000.0F),
			new Movie("3","Transporter","Jason Statham",300000.0F),
			new Movie("4","Hacksaw Ridge","Andrew Garfield",900000.0F),
			new Movie("5","Titanic","Leonardo Dicaprio",1200000.0F),
			new Movie("6","The Dark Knight Rises","Christian Bale",850000.0F)
			));
	
	public List<Movie> getAllMovies() {
		 return movies;
	}
	
	public Movie getMovie(String id) {
		int k=0,i=0;
		for(Movie m: movies) {
			if(m.getMovieId().equals(id))
			{
				k=i;
				break;
			}
			i++;
		}
		return movies.get(i);
	}

}
