package com.rest.assign;

import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

@Path("/number")  
public class NumberMultiplier {

	
	@POST  
    @Path("/double")  
    public Response addUser(  
        @FormParam("number") long id 
       ) {  
   
        return Response.status(200)  
            .entity(id*2)  
            .build();  
    }  
}
